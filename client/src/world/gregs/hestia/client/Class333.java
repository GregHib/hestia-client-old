package world.gregs.hestia.client;
/* Class333 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class333 {
    static int anInt4149;
    static float aFloat4152 = 1.0F;
    static int anInt4153;
    static int anInt4154;
    static int anInt4155;
    protected Interface15_Impl1 anInterface15_Impl1_4147;
    protected boolean aBoolean4148;
    protected boolean aBoolean4150;
    protected Interface15_Impl1 anInterface15_Impl1_4151;

    Class333(boolean bool) {
        aBoolean4148 = bool;
    }

    final void method3846(byte b) {
        if (anInterface15_Impl1_4147 != null) {
            anInterface15_Impl1_4147.method38(false);
        }
        if (b > 30) {
            anInt4154++;
            aBoolean4150 = false;
        }
    }

    final boolean method3847(int i) {
        if (i != 7997) {
            method3847(-31);
        }
        anInt4153++;
        return aBoolean4150 && !aBoolean4148;
    }
}
