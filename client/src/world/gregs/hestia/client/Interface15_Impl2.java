package world.gregs.hestia.client;
/* Interface15_Impl2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

import jaclib.memory.MemoryBuffer;
import jaclib.memory.Source;

interface Interface15_Impl2 extends Interface15 {
    boolean method36(int i, int i_0_, Source source, int i_1_);

    boolean method37(int i, int i_2_, int i_3_);

    void method38(boolean bool);

    boolean method39(int i);

    MemoryBuffer method40(int i, boolean bool);
}
