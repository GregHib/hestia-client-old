/* Peer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.peer;

abstract class Peer
{
	protected PeerReference reference;
	
	protected long a() {
		return reference.b(0);
	}
	
	public final boolean a(byte b) {
		if (b > -61) {
			reference = null;
		}
		return reference.a(0);
	}
	
	private static final native void init(Class var_class);
	
	protected Peer() {
		/* empty */
	}
	
	static {
		init(PeerReference.class);
	}
}
